//
//  LoginViewController.swift
//  NugrohoChat
//
//  Created by Nugroho Arief Widodo on 01/02/22.
//

import UIKit

class LoginViewController: UIViewController {
    @IBOutlet var serverField: UITextField!
    @IBOutlet var nickField: UITextField!
    @IBOutlet var serverButton: UIButton!
    @IBOutlet var loginButton: UIButton!
    private var isServerOn = false

    @IBAction func didTapServer(_ sender: UIButton) {

        if isServerOn {
            stopServer()
        } else {
            startServer()
        }
    }

    @IBAction func didTapLogin(_ sender: UIButton) {
        if UserSession.shared.isLoggedIn {
            logout()
        } else {
            login()
        }

    }

    func login() {

        guard let nick = nickField.text, !nick.isEmpty, let server = serverField.text else {
            return
        }

        do {
            try DatabaseManager.shared.openOrCreateDatabaseForUser()
            try DatabaseManager.shared.startP2PReplicationWithUserDatabaseToRemotePeer(server, handler: { status in
                if status == .Error || status == .Disconnected {
                    DispatchQueue.main.asyncAfter(deadline: .now()+5) {
                        self.login()
                    }
                    return
                }
            })
        } catch let error {
            print(error.localizedDescription)
            return
        }

        DispatchQueue.main.async {
            UserSession.shared.id = nick
            UserSession.shared.isLoggedIn = true
            self.serverButton.isEnabled = false
            self.serverField.isEnabled = false
            self.nickField.isEnabled = false

            // Custom color
            let redColor = UIColor.systemRed
            // create the attributed colour
            let attributedStringColor = [NSAttributedString.Key.foregroundColor : redColor];
            // create the attributed string
            let attributedString = NSAttributedString(string: "Logout", attributes: attributedStringColor)
            // Set the label
            self.loginButton.setAttributedTitle(attributedString, for: .normal)
        }
    }

    func logout() {
        if let peer = serverField.text {
            DatabaseManager.shared.stopP2PReplicationWithUserDatabaseToRemotePeer(peer)
        }
        DatabaseManager.shared.closeDatabaseForCurrentUser()
        DispatchQueue.main.async {
            self.serverButton.isEnabled = true
            self.serverField.isEnabled = true
            self.nickField.isEnabled = true
            UserSession.shared.isLoggedIn = false
            UserSession.shared.id = defaultID
            // Custom color
            let blueColor = UIColor.systemBlue
            // create the attributed colour
            let attributedStringColor = [NSAttributedString.Key.foregroundColor : blueColor];
            // create the attributed string
            let attributedString = NSAttributedString(string: "Login", attributes: attributedStringColor)
            // Set the label
            self.loginButton.setAttributedTitle(attributedString, for: .normal)
        }
    }

    func stopServer() {

        DatabaseManager.shared.closeDatabaseForCurrentUser()
        DispatchQueue.main.async {
            self.loginButton.isEnabled = true
            self.serverField.isEnabled = true
            self.nickField.isEnabled = true
            self.isServerOn = false
            UserSession.shared.isLoggedIn = false
            UserSession.shared.id = defaultID
            // Custom color
            let greenColor = UIColor.systemGreen
            // create the attributed colour
            let attributedStringColor = [NSAttributedString.Key.foregroundColor : greenColor];
            // create the attributed string
            let attributedString = NSAttributedString(string: "Start server", attributes: attributedStringColor)
            // Set the label
            self.serverButton.setAttributedTitle(attributedString, for: .normal)
        }
    }

    fileprivate func printWebsockets(_ urls: [URL]) {
        let url = urls[0]
        let hostName = getIPAddress()
        let portVal:UInt16 = UInt16(url.port!)

        var urlc = URLComponents.init()
        urlc.scheme = "ws"
        urlc.host = hostName
        urlc.port = Int(portVal)
        urlc.path = url.path

        print("Started service for database on \(urlc)")
    }

    private func startServer() {
        guard let nick = nickField.text, !nick.isEmpty, !isServerOn else {
            return
        }

        do {
            try DatabaseManager.shared.openOrCreateDatabaseForUser()
            try DatabaseManager.shared.initWebsocketsListenerForUserDb()
            try DatabaseManager.shared.startWebsocketsListenerForUserDb(handler: { urls, error in
                guard error == nil else {
                    return
                }
                guard let urls = urls else {
                    return
                }

                DispatchQueue.main.async {
                    UserSession.shared.id = nick
                    UserSession.shared.isLoggedIn = true
                    self.isServerOn = true
                    self.loginButton.isEnabled = false
                    self.serverField.isEnabled = false
                    self.nickField.isEnabled = false
                    // Custom color
                    let redColor = UIColor.systemRed
                    // create the attributed colour
                    let attributedStringColor = [NSAttributedString.Key.foregroundColor : redColor];
                    // create the attributed string
                    let attributedString = NSAttributedString(string: "Stop server", attributes: attributedStringColor)
                    // Set the label
                    self.serverButton.setAttributedTitle(attributedString, for: .normal)
                    self.printWebsockets(urls)
                }
            })
        } catch (let err) {
            print(err.localizedDescription)
            return
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
}



extension LoginViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if !string.isEmpty {
            UserSession.shared.id = string
        }
        return true
    }
}
