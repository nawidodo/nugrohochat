//
//  ChatViewController.swift
//  SSChat
//
//  Created by Sai Sandeep on 03/12/19.
//  Copyright © 2019 Nugroho Arief Widodo. All rights reserved.
//

import UIKit


class CellIds {
    
    static let senderCellId = "senderCellId"
    
    static let receiverCellId = "receiverCellId"
}

struct Chat: Hashable {
    let id: String
    let owner: String
    let message: String
    let time: UnixTimestamp

    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}


class ChatViewController: UIViewController {

    var presenter: ChatPresenter?
    
    var bottomHeight: CGFloat {
        guard #available(iOS 11.0, *),
            let window = UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .compactMap({$0 as? UIWindowScene})
                .first?.windows
                .filter({$0.isKeyWindow}).first else {
                return 0
        }
        return window.safeAreaInsets.bottom
    }
    
    var items = [Chat]()
    
    var tableView: UITableView = {
        let v = UITableView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var inputTextView: InputTextView = {
        let v = InputTextView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    var inputTextViewBottomConstraint: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
        self.navigationItem.title = "Chat Room"
        self.view.backgroundColor = UIColor.white
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name:UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name:UIResponder.keyboardWillHideNotification, object: nil)
        self.setupViews()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if !UserSession.shared.isLoggedIn {
//            reloadData()
//        } else {
//        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.loadAllChats()
    }

    func scrollToBotom() {
        DispatchQueue.main.async {
            guard !self.items.isEmpty else {
                return
            }
            let lastIndex = self.items.count - 1
            self.tableView.scrollToRow(at: IndexPath(item: lastIndex, section: 0), at: .bottom, animated: true)
        }
    }

    func reloadData() {
        tableView.reloadData()
        scrollToBotom()
    }

    func updateCollectionContentInset() {
        let contentSize = tableView.contentSize
        var contentInsetTop = tableView.bounds.size.height

            contentInsetTop -= contentSize.height
            if contentInsetTop <= 0 {
                contentInsetTop = 0
        }
        tableView.contentInset = UIEdgeInsets(top: contentInsetTop,left: 0,bottom: 0,right: 0)
    }
    
    func setupViews() {
        self.view.addSubview(tableView)
        tableView.edges([.left, .right, .top], to: self.view, offset: .zero)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: CellIds.receiverCellId)
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: CellIds.senderCellId)
        tableView.separatorStyle = .none
        tableView.keyboardDismissMode = .onDrag
        
        tableView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tableViewTapped(recognizer:))))
        
        self.view.addSubview(inputTextView)
        inputTextView.edges([.left, .right], to: self.view, offset: .zero)
        inputTextViewBottomConstraint = inputTextView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        inputTextViewBottomConstraint.isActive = true
        inputTextView.topAnchor.constraint(equalTo: tableView.bottomAnchor).isActive = true
        inputTextView.delegate = self
        
    }
    
    @objc func keyboardWillShow(notification:NSNotification){
        let userInfo = notification.userInfo!
        if var keyboardFrame  = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, let keyboardAnimationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval {
            keyboardFrame = self.view.convert(keyboardFrame, from: nil)
            let oldOffset = self.tableView.contentOffset
            self.inputTextViewBottomConstraint.constant = -keyboardFrame.height + bottomHeight
            UIView.animate(withDuration: keyboardAnimationDuration) {
                self.view.layoutIfNeeded()
                self.tableView.setContentOffset(CGPoint(x: oldOffset.x, y: oldOffset.y + keyboardFrame.height - self.bottomHeight), animated: false)
            }
        }
    }
    
    @objc func keyboardWillHide(notification:NSNotification){
        let userInfo = notification.userInfo!
        if var keyboardFrame  = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue, let keyboardAnimationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval, let curve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey] as? UInt {
            keyboardFrame = self.view.convert(keyboardFrame, from: nil)
            self.inputTextViewBottomConstraint.constant = 0
            let oldOffset = self.tableView.contentOffset
            UIView.animate(withDuration: keyboardAnimationDuration, delay: 0, options: UIView.AnimationOptions(rawValue: curve), animations: {
                self.view.layoutIfNeeded()
                self.tableView.setContentOffset(CGPoint(x: oldOffset.x, y: oldOffset.y - keyboardFrame.height + self.bottomHeight), animated: false)
            }, completion: nil)
        }
    }
    
    @objc func tableViewTapped(recognizer: UITapGestureRecognizer) {
        self.inputTextView.textView.resignFirstResponder()
    }
}

extension ChatViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        if  item.owner != UserSession.shared.id {
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.receiverCellId, for: indexPath) as? CustomTableViewCell {
                cell.selectionStyle = .none
                cell.textView.text = item.message
                cell.showTopLabel = true
                cell.bottomLabel.text = item.time.date.dateAndTimetoString()
                cell.topLabel.text = item.owner
                return cell
            }
        }
        else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: CellIds.senderCellId, for: indexPath) as? CustomTableViewCell {
                cell.selectionStyle = .none
                cell.textView.text = item.message
                cell.bottomLabel.text = item.time.date.dateAndTimetoString()
                return cell
            }
        }
        return UITableViewCell()
    }
}

extension ChatViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

extension ChatViewController: InputTextViewDelegate {
    func didPressSendButton(_ text: String, _ sender: UIButton, _ textView: UITextView) {
        textView.text = nil
        let chat = Chat(id: UUID().uuidString, owner: UserSession.shared.id, message: text, time: Date().unixTimestamp)
        presenter?.save(chat: chat)
    }
}

extension ChatViewController {
    func updateUI(with chats: [Chat]) {
//        let newCount = chats.count
//        let currentCount = items.count
//        let distance = newCount - currentCount
//        self.items = chats
//        var rows = [IndexPath]()
//        if distance > 0 {
//            for i in 1...distance {
//                let path = IndexPath(row: currentCount-1+i, section: 0)
//                rows.append(path)
//            }
//            tableView.beginUpdates()
//            tableView.insertRows(at: rows, with: .bottom)
//            tableView.endUpdates()
//            scrollToBotom()
//        }
        items = chats
        reloadData()
    }
}



/*

 do {
     try DatabaseManager.shared.initWebsocketsListenerForUserDb()
     try DatabaseManager.shared.startWebsocketsListenerForUserDb(handler: { urls, error in
         guard let urls = urls else {
             return
         }
         let url = urls[0]
         let hostName = getIPAddress()
         let portVal:UInt16 = UInt16(url.port!)

         var urlc = URLComponents.init()
         urlc.scheme = "ws"
         urlc.host = hostName
         urlc.port = Int(portVal)
         urlc.path = url.path

         print("Started service for database on \(urlc)")

         guard let error = error else {
             return
         }
         print(error.localizedDescription)
     })
 } catch (let err) {
     print(err.localizedDescription)
 }

 */
