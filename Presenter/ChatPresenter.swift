//
//  ChatPresenter.swift
//  NugrohoChat
//
//  Created by Nugroho Arief Widodo on 02/02/22.
//

import Foundation
import CouchbaseLiteSwift

class ChatPresenter: NSObject {
    weak var vc: ChatViewController?
    var queryToken: ListenerToken?

    func viewDidLoad() {
        try? DatabaseManager.shared.openOrCreateDatabaseForUser()
    }

    func loadAllChats() {
        var chats = [Chat]()
        guard let db = DatabaseManager.shared.db else {
            return
        }
        let query = QueryBuilder
               .select(SelectResult.all())
               .from(DataSource.database(db))

        queryToken = query.addChangeListener({ change in
            guard let results = change.results else { return }
            chats = []
            for result in results {
                if let record = result.dictionary(forKey: "chatDB"){
                    var owner =  record.string(forKey: "owner")! // <.>
                    if owner == defaultID {
                        owner = UserSession.shared.id
                    }
                    let message =  record.string(forKey: "message")!
                    let time  =  record.int(forKey: "time") // <.>
                    let id = record.string(forKey: "id")!
                    let chatRecord = Chat(id: id, owner: owner, message: message, time: time) // <.>
                    chats.append(chatRecord)
                }
            }
            DispatchQueue.main.async {
                self.vc?.updateUI(with: chats)
            }
        })

    }

    func save(chat: Chat) {
        guard let db = DatabaseManager.shared.db else {
            fatalError("db is not initialized at this point!")
        }

        let mutableDoc = MutableDocument(id: chat.id)
        mutableDoc.setString(chat.id, forKey: "id")
        mutableDoc.setString(chat.owner, forKey: "owner")
        mutableDoc.setString(chat.message, forKey: "message")
        mutableDoc.setInt(chat.time, forKey: "time")

        do {
            // This will create a document if it does not exist and overrite it if it exists
            // Using default concurrency control policy of "writes always win"
            try db.saveDocument(mutableDoc)
        }
        catch (let error){
            print(error.localizedDescription)
        }
    }
}
