//
//  DatabaseManager.swift
//
//
//  Created by Nugroho Arief Widodo on 2/19/18.
//  Copyright © Nugroho Arief Widodo. All rights reserved.
//

import Foundation
import CouchbaseLiteSwift

typealias Completion = (Error?) -> Void
typealias ReplicatorCompletion = (PeerConnectionStatus) -> Void
typealias PeerHost = String
var listenPort: UInt16 = 56069

enum PeerConnectionStatus {
    case Connected
    case Connecting
    case Disconnected
    case Busy
    case Error
    case Unknown
}

class DatabaseManager {
    
    // public
    private(set) var db: Database?
    var dbChangeListenerToken: ListenerToken?
    var lastError: Error?

    // db name
    fileprivate let kDBName: String = "chatDB"
    fileprivate var _replicatorsToPeers: [PeerHost: Replicator] = [PeerHost: Replicator]()
    fileprivate var _applicationDocumentDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last
    fileprivate var _websocketListener: URLEndpointListener?
    fileprivate var _replicatorListenersToPeers: [PeerHost: ListenerToken] = [PeerHost: ListenerToken]()

    static let shared: DatabaseManager = {
        
        let instance = DatabaseManager()
        instance.initialize()
        return instance
    }()
    
    func initialize() {
        enableCrazyLevelLogging()
    }
    // Don't allow instantiation . Enforce singleton
    private init() {
        
    }
    
    deinit {
        // Stop observing changes to the database that affect the query
        do {
            try self.db?.close()
        }
        catch  {
            
        }
    }
    
}

// MARK: Local Operation
extension DatabaseManager {
    // tag::openOrCreateDatabaseForUser[]
    func openOrCreateDatabaseForUser(_ handler: Completion? = nil) throws {
    // end::openOrCreateDatabaseForUser[]
        do {
            // tag::dbconfig[]
            let options = DatabaseConfiguration()
            guard let defaultDBPath = _applicationDocumentDirectory else {
                fatalError("Could not open Application Support Directory for app!")
            }
            // Create a folder for the logged in user if one does not exist
            //let userFolderUrl = defaultDBPath.appendingPathComponent(user, isDirectory: true)
            let userFolderPath = defaultDBPath.path
            let fileManager = FileManager.default
            if !fileManager.fileExists(atPath: userFolderPath) {
                try fileManager.createDirectory(atPath: userFolderPath,
                                                withIntermediateDirectories: true,
                                                attributes: nil)
                
            }
            // Set the folder path for the CBLite DB
            options.directory = userFolderPath
            // end::dbconfig[]
   
            print("Will open/create DB  at path \(userFolderPath)")
            // tag::dbcreate[]
            // Create a new DB or get handle to existing DB at specified path
            db = try Database(name: kDBName, config: options)
            
            // end::dbcreate[]
            
            // register for DB change notifications
            self.registerForDatabaseChanges()
            
            handler?(nil)
        }catch {
            
            lastError = error
            handler?(lastError)
        }
    }
    

    // close database for user
     func closeDatabaseForCurrentUser() {

        print(#function)
        // Get handle to DB  specified path

        if let userDb = db {

            // Remove listeners for database
            deregisterForDatabaseChanges()

            // Remove listeners for replicators
            deregisterEventsForAllReplicators()

            stopWebsocketsListenerForUserDb()
            
            let group = DispatchGroup()
            group.enter()

            DispatchQueue.init(label: "com.db.list-sync", qos: .userInitiated, attributes: .concurrent, autoreleaseFrequency: .inherit, target: nil) .async
            {
                defer {
                    group.leave()
                    self.db = nil
                }
                do {
                    try userDb.close()
                }
                catch {
                    print("Error while attempting to close database \(error)")
                }

            }
            // wait
            let result = group.wait(timeout: DispatchTime.now()+8)

            if result == .timedOut {
                print("Failed to close database and replicators up succesfully!")
            } else {
                for val in _replicatorsToPeers.values {
                    print ("Replicator Status is \(val.status)")
                }
                print("Succesfully closed database, and associated listeners/replicators as appropriate")
            }
            _replicatorsToPeers.removeAll()
        }

    }
    
    // tag::registerForDatabaseChanges[]
    fileprivate func registerForDatabaseChanges() {
        // end::registerForDatabaseChanges[]
        
        // tag::adddbchangelistener[]
        // Add database change listener
        dbChangeListenerToken = db?.addChangeListener({ [weak self](change) in
            guard let `self` = self else {
                return
            }
            for docId in change.documentIDs   {
                let doc = self.db?.document(withID: docId)
                if doc == nil {
                    print("Document was deleted")
                }
                else {
                    print("Document was added/updated")
                }
            }
        })
        // end::adddbchangelistener[]
    }
    
    // tag::deregisterForDatabaseChanges[]
    fileprivate func deregisterForDatabaseChanges() {
        // end::deregisterForDatabaseChanges[]
        
        // tag::removedbchangelistener[]
        // Add database change listener
        if let dbChangeListenerToken = self.dbChangeListenerToken {
            db?.removeChangeListener(withToken: dbChangeListenerToken)
        }
   
        // end::removedbchangelistener[]
    }
}

// MARK: Utils
extension DatabaseManager {
    
    fileprivate func enableCrazyLevelLogging() {
        Database.log.console.domains = .all
        Database.log.console.level = .debug
    }
    
}

// MARK: Pasive Web Socket
extension DatabaseManager {
    func initWebsocketsListenerForUserDb() throws {
        guard let db = db else {
            throw ListDocError.DatabaseNotInitialized
        }

        if _websocketListener != nil  {
            print("Listener already initialized")
            return
        }

        //tag::InitListener[]
        // Include websockets listener initializer code
        let listenerConfig = URLEndpointListenerConfiguration(database: db) // <1>
        listenerConfig.disableTLS  = true
        listenerConfig.tlsIdentity = nil
        listenerConfig.enableDeltaSync = true // <3>
        listenerConfig.authenticator = nil
        listenerConfig.port = listenPort

        _websocketListener = URLEndpointListener(config: listenerConfig)
        //end::InitListener[]
    }

    func startWebsocketsListenerForUserDb(handler: @escaping(_ urls:[URL]?, _ error: Error?)->Void) throws{
       print(#function)
       guard let websocketListener = _websocketListener else {
           throw ListDocError.WebsocketsListenerNotInitialized
           }
       //tag::StartListener[]
       DispatchQueue.global().sync {
           do {
               try websocketListener.start()
               handler(websocketListener.urls,nil)
           }
           catch {
               handler(nil, error)
           }

       }

       //end::StartListener[]
   }

    //tag::StopListener[]
    func stopWebsocketsListenerForUserDb() {
        print(#function)
        guard let websocketListener = _websocketListener else {
            return
        }
        websocketListener.stop()
        _websocketListener = nil
    }
    //end::StopListener[]

}

// MARK: Peer-to-peer Active Replicator
extension DatabaseManager {

    func startP2PReplicationWithUserDatabaseToRemotePeer(_ peer: PeerHost, handler:@escaping(_ status: PeerConnectionStatus)->Void) throws{
        guard let userDb = db else {
             throw ListDocError.DatabaseNotInitialized

        }

        var replicatorForUserDb = _replicatorsToPeers[peer]
        //tag::StartReplication[]
        if replicatorForUserDb == nil {
            // Start replicator to connect to the URLListenerEndpoint
            let target = URL(string: "ws://\(peer):\(listenPort)/\(kDBName)")
            guard let targetUrl = target else {
                throw ListDocError.URLInvalid
            }
            print("\(#function) with \(targetUrl)")


            let config = ReplicatorConfiguration.init(database: userDb, target: URLEndpoint.init(url:targetUrl)) //<1>

            config.replicatorType = .pushAndPull
            config.continuous =  true
            config.acceptOnlySelfSignedServerCertificate = true
            config.authenticator = nil

            replicatorForUserDb = Replicator.init(config: config) //<4>
            _replicatorsToPeers[peer] = replicatorForUserDb

          }
        if let pushPullReplListenerForUserDb = registerForEventsForReplicator(replicatorForUserDb,handler:handler) {
            _replicatorListenersToPeers[peer] = pushPullReplListenerForUserDb

        }

        replicatorForUserDb?.start() //<5>
        handler(PeerConnectionStatus.Connecting)
         //end::StartReplication[]
      }

    func stopP2PReplicationWithUserDatabaseToRemotePeer(_ peer:PeerHost, shouldRemove:Bool = true, handler: ReplicatorCompletion? = nil) {
        guard let replicator = _replicatorsToPeers[peer] else {
            print("Replicator does not exist!! ")
            handler?(.Error)
            return
        }
        //tag::StopReplication[]
        if let listener = _replicatorListenersToPeers[peer] {
            replicator.removeChangeListener(withToken: listener)
            _replicatorListenersToPeers.removeValue(forKey: peer)
        }

        replicator.stop()
        //end::StopReplication[]
        if shouldRemove {
            _replicatorsToPeers.removeValue(forKey: peer)
            handler?(.Disconnected)
        }
        else {
            handler?(.Disconnected)
        }
    }
}

// MARK :  Replicator Status Events
extension DatabaseManager {

    fileprivate func registerForEventsForReplicator(_ replicator: Replicator?, handler: @escaping(_ status: PeerConnectionStatus)->Void )-> ListenerToken? {
        let pushPullReplListenerForUserDb = replicator?.addChangeListener({ (change) in

                   let s = change.status
                   if s.error != nil {
                       handler(PeerConnectionStatus.Error)
                       return
                   }

                   switch s.activity {
                   case .connecting:
                       print("Replicator Connecting to Peer")
                       handler(PeerConnectionStatus.Connecting)
                   case .idle:
                       print("Replicator in Idle state")
                       handler(PeerConnectionStatus.Connected)
                   case .busy:
                       print("Replicator in busy state")
                       handler(PeerConnectionStatus.Busy)
                   case .offline:
                       print("Replicator in offline state")
                   case .stopped:
                       print("Completed syncing documents")
                       handler(PeerConnectionStatus.Error)

                       @unknown default:
                           print("Failed syncing documents")
                           handler(PeerConnectionStatus.Unknown)
                   }

                   if s.progress.completed == s.progress.total {
                       print("All documents synced")
                   }
                   else {
                       print("Documents \(s.progress.total - s.progress.completed) still pending sync")
                   }
               })
        return pushPullReplListenerForUserDb

    }

    fileprivate func deregisterEventsForAllReplicators() {
        print(#function)
        for (peer, replicator) in _replicatorsToPeers {
            if let listener = _replicatorListenersToPeers[peer] {
                replicator.removeChangeListener(withToken: listener)
                _replicatorListenersToPeers.removeValue(forKey: peer)
            }
        }
    }
}

// MARK: CRUD
extension DatabaseManager {
    
}



