//
//  UserSession.swift
//  NugrohoChat
//
//  Created by Nugroho Arief Widodo on 01/02/22.
//

import UIKit

var defaultID = "Anonymous" + UIDevice.current.identifierForVendor!.uuidString

class UserSession: NSObject {
    static let shared = UserSession()
    var id = defaultID
    var isLoggedIn = false
}
